import os
import time
from utils import *
import re
import tabulate
from operator import itemgetter
from db import DB


# run in macrobench dir

# PIN_POLICY="0-9.40-49.10-19.50-59.20-29.60-69.30-39.70-79"
# PIN_POLICY = "0-1"
# THREADS = 1
# WAREHOUSES="-n80"
# WAREHOUSES = "-n2"
# LOGS_DIR = "run_logs"
# BASE_COMMAND = "env LD_PRELOAD=../lib/libjemalloc-5.0.1-25.so ./bin/{0} -t{1} {2} -pin {3} > {4}"
def run_tests(logs_dir_prefix, warehouses, threads, pin, test_dir, db, iterations=1):
    for i in range(0,iterations):
        logs_dir =  logs_dir_prefix + str(i)
        log("running iteration {0} out of {1}, log dir: {2}", i + 1, iterations, logs_dir)
        runner = Runner(logs_dir, warehouses, threads, pin, test_dir, db)
        runner.run()

def read_logs(logs_dir, warehouses, threads, pin, test_dir, db):
        runner = Runner(logs_dir, warehouses, threads, pin, test_dir, db)
        runner.read_logs(runner.get_tests())

class Runner():

    def __init__(self, logs_dir, warehouses, threads, pin_policy, tests_dir, db):
        self.LOGS_DIR = logs_dir
        self.WAREHOUSES = "-n{0}".format(warehouses)
        self.PIN_POLICY = pin_policy
        self.THREADS = threads
        self.tests_dir = tests_dir
        self.BASE_COMMAND_FORMAT = "env LD_PRELOAD=../lib/libjemalloc-5.0.1-25.so ./bin/{0} -t{1} {2} -pin {3} > {4}"
        self.db = db
        if self.db is not None:
            self.db.create_table()

    def read_logs(self, tests):
        table = []
        for test in tests:
            log_file = self.get_log_file_name(test)
            stats = self.process_log(test, log_file)
            if stats is None:
                continue
            run_time = stats['run_time']
            ix_total_time = stats['ixTotalTime']
            dbx_time = run_time - ix_total_time
            table += [[test, run_time, ix_total_time, dbx_time]]

        table = sorted(table, key=itemgetter(1))
        log(tabulate.tabulate(table, headers=["TEST", "RUN TIME", "IX TIME", "DB TIME"]))

    def process_log(self, test_name, log_file):
        with open(log_file, "r+") as f:
            result = f.read()

        if not "summary" in result:
            log("no summary for test: {0}", test_name)
            return None

        tokens = re.sub(r"\s+", "", result.split("[summary]")[1]).split(",")
        stats = {}
        for token in tokens:
            values = token.split("=")
            log("setting {0} to be {1}", values[0], values[1])
            stats[values[0]] = float(values[1])

        stats['dbxTime'] = stats['run_time'] - stats['ixTotalTime']
        stats['test_name'] = test_name

        log("{0}", pprint_item(stats))

        if self.db is not None:
            self.db.insert(stats)

        return stats

    def get_log_file_name(self, test):
        return self.LOGS_DIR + "/" + test + ".log"

    def get_tests(self):
        return list(filter(lambda x: x.startswith("rundb"), os.listdir(self.tests_dir)))

    def run(self):
        if not os.path.exists(self.LOGS_DIR):
            log("creating log dir: {0}", self.LOGS_DIR)
            os.makedirs(self.LOGS_DIR)

        tests = self.get_tests()
        log("found tests: {0}", tests)
        for test in tests:
            self.run_test(test)

        self.read_logs(tests)

    def run_test(self, test):
        log("running test: {0}", test)
        warehouses = ""
        if "TPCC" in test:
            warehouses = self.WAREHOUSES
            log("setting warehouses to {0}", warehouses)
        log_file = self.get_log_file_name(test)
        command = self.BASE_COMMAND_FORMAT.format(test, self.THREADS, warehouses, self.PIN_POLICY, log_file)
        log("running command: {0}", command)
        os.system(command)
