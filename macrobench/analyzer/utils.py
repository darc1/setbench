import pprint

def log(string, *args):
    print(string.format(*args))

def pprint_item(item):
    return pprint.pformat(item)