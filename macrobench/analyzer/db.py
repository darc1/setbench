import sqlite3

from utils import log


class DB():

    def __init__(self, db_name):
        self.conn = sqlite3.connect(db_name)


    def create_table(self):
        self.conn.execute('''CREATE TABLE IF NOT EXISTS results
                              (
                              test_name TEXT,
                              abort_cnt REAL,
                              cycle_detect REAL,
                              deadlock_cnt REAL,
                              debug1 REAL,
                              debug2 REAL,
                              debug3 REAL,
                              debug4 REAL,
                              debug5 REAL,
                              descriptor_size REAL,
                              dl_detect_time REAL,
                              dl_wait_time REAL,
                              ixNumContains REAL,
                              ixNumInsert REAL,
                              ixNumRangeQuery REAL,
                              ixThroughput REAL,
                              ixTimeContains REAL,
                              ixTimeInsert REAL,
                              ixTimeRangeQuery REAL,
                              ixTotalOps REAL,
                              ixTotalTime REAL,
                              latency REAL,
                              node_size REAL,
                              nthreads REAL,
                              run_time REAL,
                              throughput REAL,
                              time_abort REAL,
                              time_cleanup REAL,
                              time_index REAL,
                              time_man REAL,
                              time_query REAL,
                              time_ts_alloc REAL,
                              time_wait REAL,
                              txn_cnt REAL
                              );''')
        self.conn.commit()

    def insert(self, values_dict):

        values = []
        for key in cols:
            values.append(values_dict[key])

        log("adding values {0}", values)

        self.conn.execute('''INSERT INTO results
                          values(?, ?, ?, ?, ?, ?, ?, ?, ?, ?,
                          ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?,
                          ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)''', values)
        self.conn.commit()

    def close(self):
        self.conn.close()

cols = ["test_name",
"abort_cnt",
"cycle_detect",
"deadlock_cnt",
"debug1",
"debug2",
"debug3",
"debug4",
"debug5",
"descriptor_size",
"dl_detect_time",
"dl_wait_time",
"ixNumContains",
"ixNumInsert",
"ixNumRangeQuery",
"ixThroughput",
"ixTimeContains",
"ixTimeInsert",
"ixTimeRangeQuery",
"ixTotalOps",
"ixTotalTime",
"latency",
"node_size",
"nthreads",
"run_time",
"throughput",
"time_abort",
"time_cleanup",
"time_index",
"time_man",
"time_query",
"time_ts_alloc",
"time_wait",
"txn_cnt"]
