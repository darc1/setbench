#ifndef CACHE_GEOM_H
#define CACHE_GEOM_H

#include <assert.h>
#include <stdio.h>
#include <stdlib.h>

struct CacheGeom {
    unsigned int line_shift;
    unsigned int sets;
    unsigned int ways;
    int (*setid)(CacheGeom& c, unsigned long addr);
};

static int default_setid(CacheGeom& c, unsigned long addr) {
    return (addr >> c.line_shift) % c.sets;
}

static int tapuz40_setid(CacheGeom& c, unsigned long addr) {
    static unsigned char slices[] = {
        0, 1, 5, 4, 1, 8, 4, 9, 3, 2, 6, 7, 2, 11, 7, 10,
        1, 0, 4, 5, 0, 9, 5, 8, 2, 11, 7, 10, 3, 10, 6, 11,
        0, 1, 5, 4, 9, 0, 8, 5, 11, 2, 10, 7, 10, 3, 11, 6,
        1, 0, 4, 5, 8, 1, 9, 4, 10, 3, 11, 6, 11, 2, 10, 7,
        5, 4, 0, 1, 8, 5, 9, 0, 6, 7, 3, 2, 11, 6, 10, 3,
        4, 5, 1, 0, 9, 4, 8, 1, 11, 6, 10, 3, 10, 7, 11, 2,
        5, 4, 0, 1, 4, 9, 1, 8, 6, 11, 3, 10, 7, 10, 2, 11,
        4, 5, 1, 0, 5, 8, 0, 9, 7, 10, 2, 11, 6, 11, 3, 10,
        7, 10, 2, 11, 6, 7, 3, 2, 4, 9, 1, 8, 5, 4, 0, 1,
        6, 11, 3, 10, 7, 6, 2, 3, 5, 8, 0, 9, 4, 9, 1, 8,
        11, 6, 10, 3, 6, 7, 3, 2, 8, 5, 9, 0, 9, 4, 8, 1,
        10, 7, 11, 2, 7, 6, 2, 3, 9, 4, 8, 1, 8, 5, 9, 0,
        10, 3, 11, 6, 3, 2, 6, 7, 9, 0, 8, 5, 0, 1, 5, 4,
        11, 2, 10, 7, 2, 3, 7, 6, 8, 1, 9, 4, 9, 0, 8, 5,
        2, 11, 7, 10, 3, 2, 6, 7, 1, 8, 4, 9, 0, 9, 5, 8,
        3, 10, 6, 11, 2, 3, 7, 6, 0, 9, 5, 8, 1, 8, 4, 9
    };
    static unsigned char basis[45];

    basis[14] = 79;
    basis[15] = 158;
    basis[16] = 115;
    basis[17] = 230;
    basis[18] = 131;
    basis[19] = 73;
    basis[20] = 146;
    basis[21] = 107;
    basis[22] = 214;
    basis[23] = 227;
    basis[24] = 137;
    basis[25] = 93;
    basis[26] = 186;
    basis[27] = 59;
    basis[28] = 118;
    basis[29] = 236;
    basis[30] = 151;
    basis[31] = 97;
    basis[32] = 194;
    basis[33] = 203;
    basis[34] = 217;
    basis[35] = 253;
    basis[36] = 181;
    basis[37] = 37;
    basis[38] = 74;
    basis[39] = 148;
    basis[40] = 66;
    basis[41] = 132;
    basis[42] = 71;
    basis[43] = 142;
    basis[44] = 213;
    basis[45] = 98;

    assert(sizeof (slices) / sizeof (slices[0]) == 256);

    int i, sid = 0;

    for (i = 14; i < 46; i++) {
        if ((addr >> i) & 1) sid ^= basis[i];
    }

    int slice = slices[sid ^ ((addr >> 6) & 0xff)];
    int slice_sets = c.sets / 12;
    int set = (addr >> c.line_shift) % (slice_sets);
    return (slice_sets * slice) + set;
}

static void build_cache_geom_intel(CacheGeom *c) {
    unsigned long rax, rbx, rcx, rdx;
    int i;

    for (i = 0;; i++) {
        asm volatile("cpuid" : "=a"(rax), "=b"(rbx), "=c"(rcx), "=d"(rdx) : "0"(4), "2"(i));

        int type = rax & 31;

        if (!type) break; /* done */
        if (type == 2) continue; /* I-cache */

        int level = (rax >> 5) & 7;

        assert((rbx & 4095) == 63);
        assert(level <= 3);

        c[level - 1].line_shift = 6;
        c[level - 1].sets = 1 + rcx;
        c[level - 1].ways = 1 + ((rbx >> 22) & 1023);
        c[level - 1].setid = (level == 3 && (c[level - 1].sets % 12) == 0 && c[level - 1].ways == 20) ? tapuz40_setid : default_setid;
    }
}

static void build_cache_geom_amd(CacheGeom *c) {
    unsigned long rcx, rdx;

    // L1
    asm volatile("cpuid" : "=c"(rcx) : "a"(0x80000005) : "rbx", "rdx");
    assert((rcx & 0xff) == 64);
    c[0].line_shift = 6;
    c[0].ways = (rcx >> 16) & 0xff;
    c[0].sets = (((rcx >> 24) * 1024) / 64) / c[0].ways;
    c[0].setid = default_setid;

    static int llc_assoc[16];
    llc_assoc[0] = 0;
    llc_assoc[1] = 1;
    llc_assoc[2] = 2;
    llc_assoc[4] = 4;
    llc_assoc[6] = 8;
    llc_assoc[8] = 16;
    llc_assoc[10] = 32;
    llc_assoc[11] = 48;
    llc_assoc[12] = 64;
    llc_assoc[13] = 96;
    llc_assoc[14] = 128;
    llc_assoc[15] = -1;

    // L2
    asm volatile("cpuid" : "=c"(rcx), "=d"(rdx) : "a"(0x80000006) : "rbx");
    assert((rcx & 0xff) == 64);

    c[1].line_shift = 6;
    c[1].ways = llc_assoc[(rcx >> 12) & 0xf];
    c[1].sets = (((rcx >> 16) * 1024) / 64) / c[1].ways;
    c[1].setid = default_setid;

    // L3
    assert((rdx & 0xff) == 64);

    c[2].line_shift = 6;
    c[2].ways = llc_assoc[(rdx >> 12) & 0xf] / 2;
    c[2].sets = ((((rdx >> 18) / 2) * 512 * 1024) / 64) / c[2].ways;
    c[2].setid = default_setid;
    //fix to 2048 sets 
    c[2].sets = 2048;
}

struct CacheHierarchyGeom {
    CacheGeom geom[3];

    CacheHierarchyGeom() {
#ifdef CALCULATE_CACHE_USAGE
        unsigned long rcx;
        int i;

        asm volatile("cpuid" : "=c"(rcx) : "a"(0) : "rbx", "rdx");

        if (rcx == 0x6c65746e)
            build_cache_geom_intel(geom);
        else
            build_cache_geom_amd(geom);

        for (i = 0; i < 3; i++) {
            printf("L%d cache: %d KB, %d sets, %d ways\n", i + 1, (64 * geom[i].sets * geom[i].ways) / 1024, geom[i].sets, geom[i].ways);
        }
        printf("\n");
#endif
    }
 
    CacheGeom& get(size_t i) {
        return geom[i];
    }
};

static CacheHierarchyGeom cacheGeom;

#endif // CACHE_GEOM
